/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef STATISTICS_SEEN
#define STATISTICS_SEEN


double
average(const double *data, const int dim);

double
variance(const double *data, const int dim, const double mean);

double
VarVariance(const double *data, const int ddim, const int sdim);

int
moments(const double *data, const int dim,
        double *ave, double *median,
        double *adev, double *mdev,
        double *sdev, double *var,
        double *skew, double *kurt,
        double *hrange, double *lrange);

double
chi_sqr_adapt(const double *data, const int num, int nbins, double *logL,
              const double ave, const double lambda,
              double (*dist_pdf)(double x, double param1, double param2),
              double (*dist_lnpdf)(double x, double param1, double param2),
              double (*dist_int)(double x, double y, double param1, double param2));

double
chi_sqr_adapt_mix(const double *data, const int num, int nbins, double *logL,
                  const double *p1, const double *p2, const double *mixp, const int nmix,
                  double (*dist_pdf)(double x, double param1, double param2),
                  double (*dist_lnpdf)(double x, double param1, double param2),
                  double (*dist_int)(double x, double y, double param1, double param2));

double
dist_logL(double (*dist_lnpdf)(const double x, const double param1, const double param2),
          const double param1, const double param2,
          const double *data, const int num);

double
chi_sqr_hist(double *data, double *freq, int num, double *logL, double ave, double lambda,
             double (*dist_pdf)(double x, double param1, double param2),
             double (*dist_lnpdf)(double x, double param1, double param2),
             double (*dist_int)(double x, double y, double param1, double param2));

double
chi_sqr_one(double *data, int num, int nbins, double *prob, double ave, double lambda,
            double (*dist)(double x, double param1, double param2));

int
dblcmp(const void *dbl1, const void *dbl2);

int
dblcmp_rev(const void *dbl1, const void *dbl2);

#endif
