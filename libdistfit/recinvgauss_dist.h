/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef RECINVGAUSS_SEEN
#define RECINVGAUSS_SEEN

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double
recinvgauss_pdf(const double x, const double mean, const double lambda);

double
recinvgauss_lnpdf(const double x, const double mean, const double lambda);

double
recinvgauss_cdf(const double x, const double mean, const double lambda);

double
recinvgauss_sdf(const double x, const double mean, const double lambda);

double
recinvgauss_int(const double x, const double y, const double mean, const double lambda);

double
recinvgauss_dev(const double mu, const double lam, const gsl_rng *r2);

double
recinvgauss_fit(const double *data, const int num, double *mean, double *lambda, double *logL);

#endif
