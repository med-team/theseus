/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef INVGAMMA_DIST_SEEN
#define INVGAMMA_DIST_SEEN

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double
invgamma_dev(const double b, const double c, const gsl_rng *r2);

double
invgamma_pdf(const double x, const double b, const double c);

double
invgamma_lnpdf(const double x, const double b, const double c);

double
invgamma_cdf(const double x, const double b, const double c);

double
invgamma_sdf(const double x, const double b, const double c);

double
invgamma_int(const double x, const double y, const double b, const double c);

double
invgamma_logL(const double *data, const int num, const double b, const double c);

double
invgamma_fit(const double *data, const int num, double *b, double *c, double *logL);

double
ExpXn(const double b, const double c, const double n);

double
ExpLogXn(const double b, const double c, const double n);

double
ExpInvXn(const double b, const double c, const double xn1);

double
invgamma_EMsmall_fit(const double *data, const int num, int missing, double *b, double *c, double *logL);

double
invgamma_bayes_fit(const double *data, const int num, double *b, double *c, double *logL);

double
invgamma1_fit(const double *data, const int num, double *b, double *nullp, double *logL);

double
invgamma_fixed_c_EM_fit(const double *data, const int num, const int missing, double *b, const double c, double *logL);

double
invgamma_fixed_c_ML_fit(const double *data, const int num, const int missing, double *b, const double c, double *logL);

double
invgamma_fixed_c_EM_fit_bayes(const double *data, const int num, const int missing, double *b, const double c, double *logL);

double
invgamma_fit_guess(const double *data, const int num, double *b, double *c, double *logL);

double
invgamma_minc_fit(const double *data, const int num, double *b, double *c, const double minc, double *logL);

double
invgamma_minc_opt_fit(const double *data, const int num, double *b, double *c, const double minc, double *logL);

double
invgamma_mode_fit(const double *data, const int num, double *b, double *c, const double mode, double *logL);

double
invgamma_eq_bc_fit(const double *data, const int num, double *b, double *c, double *logL, int init);

#endif
