/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef NORMAL_DIST_SEEN
#define NORMAL_DIST_SEEN

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double
normal_dev(const double mu, const double var, const gsl_rng *r2);

double
normal_pdf(const double x, const double mean, const double var);

double
normal_lnpdf(const double x, const double mean, const double var);

double
normal_cdf(const double x, const double mean, const double var);

double
normal_sdf(const double x, const double mean, const double var);

double
normal_int(const double x, const double y, const double mean, const double var);

double
normal_logL(const double mean, const double var);

double
normal_fit(const double *x, const int n, double *mean, double *var, double *prob);

double
normal_fit_w(const double *x, const int n, const double *wts, double *mean,
             double *var, double *logL);

void
normal_init_mix_params(const double *x, const int n, const int mixn,
                       double *mean, double *var);

#endif
