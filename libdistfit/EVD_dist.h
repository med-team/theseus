/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef EVD_FIT_SEEN
#define EVD_FIT_SEEN

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double
EVD_dev(const double mu, const double lambda, const gsl_rng *r2);

double
EVD_pdf(const double x, const double mu, const double lambda);

double
EVD_lnpdf(const double x, const double mu, const double lambda);

double
EVD_cdf(const double x, const double mu, const double lambda);

double
EVD_sdf(const double x, const double mu, const double lambda);

double
EVD_int(const double x, const double y, const double mu, const double lambda);

void
Lawless416(const double *x, const int n, const double lambda,
           double *ret_f, double *ret_df);

double
EVD_fit(const double *x, const int n, double *rmu, double *rlambda, double *prob);

#endif
