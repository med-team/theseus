/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef CAUCHY_SEEN
#define CAUCHY_SEEN

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double
cauchy_dev(const double a, const double b, const gsl_rng *r2);

double
cauchy_pdf(const double x, const double a, const double b);

double
cauchy_lnpdf(const double x, const double a, const double b);

double
cauchy_cdf(const double x, const double a, const double b);

double
cauchy_sdf(const double x, const double a, const double b);

double
cauchy_int(const double x, const double y, const double a, const double b);

double
cauchy_logL(const double a, const double b);

double
cauchy_fit(const double *x, const int n, double *ra, double *rb, double *logL);

void
evalcauchyML(const double *x, const int n,
             double *params, double *fvec, double **fjac);

int
mnewt_cauchyML(const int ntrials, const double *x, const int n,
               double *params, const double tolx, const double tolf);

#endif
