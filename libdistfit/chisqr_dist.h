/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef CHISQR_DIST_SEEN
#define CHISQR_DIST_SEEN

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double
chisqr_dev(const double nu, const double nullval, const gsl_rng *r2);

double
chisqr_pdf(const double x, const double nu, const double nullval);

double
chisqr_lnpdf(const double x, const double nu, const double nullval);

double
chisqr_cdf(const double x, const double nu, const double nullval);

double
chisqr_sdf(const double x, const double nu, const double nullval);

double
chisqr_int(const double x, const double y, const double nu, const double nullval);

double
chisqr_logL(const double nu, const double nullval);

void
evalchisqrML(const double logterm, const double nu, double *fx, double *dfx);

double
chisqr_fit(const double *data, const int num, double *nu, double *nullp, double *prob);

#endif
