/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef GAMMA_DIST_SEEN
#define GAMMA_DIST_SEEN

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double
gamma_dev(const double a, const double b, const gsl_rng *r2);

double
gamma_int_dev(const unsigned int a);

double
gamma_large_dev(const double a);

double
gamma_frac_dev(const double a);

double
gamma_pdf(const double x, const double a, const double b);

double
gamma_lnpdf(const double x, const double b, const double c);

double
gamma_cdf(const double x, const double a, const double b);

double
gamma_sdf(const double x, const double a, const double b);

double
gamma_int(const double x, const double y, const double a, const double b);

double
gamma_logL(const double b, const double c);

double
lngamma(const double xx);

double
gamma_MMfit(const double *data, const int num, double *alpha, double *theta, double *prob);

double
gamma_Stacyfit(const double *data, const int num, double *b, double *c, double *logL);

double
gamma_fit(const double *data, const int num, double *alpha, double *theta, double *logL);

void
gamma_bayes_fit_no_stats(const double *data, const int num, double *b, double *c);

void
gamma_bayes_ref_fit_no_stats(const double *data, const int num, double *b, double *c);

void
gamma_fit_no_stats(const double *data, const int num, double *b, double *c);

double
gamma1_fit(const double *data, const int num, double *b, double *nullp, double *logL);

double
gamma_fit_guess(const double *data, const int num, double *b, double *c, double *logL);

double
gamma_minc_fit(const double *data, const int num, double *b, double *c, const double minc, double *logL);

double
gamma_minc_opt_fit(const double *data, const int num, double *b, double *c, const double minc, double *logL);

double
gamma_histfit(double *data, double *freq, int num, double *b, double *c, double *logL);

#endif
