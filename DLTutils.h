/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef DLTUTILS_SEEN
#define DLTUTILS_SEEN

#include <stdio.h>

#define SCREAMS(string_val)  fprintf(stdout, "!SCREAMS! %s:%d:%s = %s\n", __FILE__, __LINE__, #string_val,  string_val);   fflush(NULL)
#define SCREAMC(char_val)    fprintf(stdout, "!SCREAMC! %s:%d:%s = % c\n", __FILE__, __LINE__, #char_val,    char_val);     fflush(NULL)
#define SCREAMD(integer_val) fprintf(stdout, "!SCREAMD! %s:%d:%s = % d\n", __FILE__, __LINE__, #integer_val, integer_val);  fflush(NULL)
#define SCREAMF(double_val)  fprintf(stdout, "!SCREAMF! %s:%d:%s = % f\n", __FILE__, __LINE__, #double_val,  double_val);   fflush(NULL)
#define SCREAME(double_val)  fprintf(stdout, "!SCREAME! %s:%d:%s = % e\n", __FILE__, __LINE__, #double_val,  double_val);   fflush(NULL)
#define SCREAMP(pointer_val) fprintf(stdout, "!SCREAMP! %s:%d:%s = % p\n", __FILE__, __LINE__, #pointer_val,  pointer_val); fflush(NULL)
#define BUFFLEN          FILENAME_MAX


double
*memsetd(double *dest, const double val, size_t len);


void
MyFree(void *p);

void
myfloath(void);

void
clrscr(void);

void
write_C_mat(const double **mat, const int dim, int precision, int wrap);

char
*mystrncpy (char *s1, const char *s2, size_t n);

char
*mystrcpy (char *s1, const char *s2);

char
*mystrcat(const char *s1, const char *s2);

void
strtolower(char *string);

void
strtoupper(char *string);

int
printfile(FILE *infile, FILE *outfile);

void
cpfile_name(char *newname, char *oldname);

void
cpfile_fp(FILE *fnew, FILE *fold);

int
email(char *address, char program, char *datafile_name, char *histfile_name);

int
CheckForFile(char *filename);

int
GetUniqFileNameNum(const char *fname, char *newfname);

FILE
*myfopen(const char *fname, const char *mode);

int
isendline(int ch);

FILE
*linefix(char *infile_name);

char
skipspace(FILE *afp);

char
skipnum(FILE *afp);

int
getfilesize(FILE *afp);

char
*slurpfile(FILE *afp);

// char
// *itoa(int value, char *string, int radix);

char
*getroot(char *filename);

int
program_check(char *program_system_call, FILE *stream, char *check_string);

#endif
