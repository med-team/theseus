/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <time.h>
#include "DLTutils.h"
#include "FragCds.h"
#include "QuarticHornFrag.h"
#include "FragDist.h"


void
FragDist(CdsArray *cdsA, int fraglen, int center_ca)
{
    int             coord1, coord2;
    int             i, k, offset, tmp, num, count;
    FragCds        *frag1 = NULL, *frag2 = NULL;
    double         *coeff = NULL;
    double          var;
    FILE           *distfile = NULL, *distfile2 = NULL;
    double          biggest;
    double         *array = NULL;

    distfile = fopen("frags_dist.txt", "w");
    distfile2 = fopen("frags_dist2.txt", "w");
    coeff = (double *) calloc(5, sizeof(double));

    frag1 = FragCdsAlloc(fraglen);
    frag2 = FragCdsAlloc(fraglen);

    offset = (fraglen - 1) / 2;

    num = cdsA->vlen * cdsA->cnum * (cdsA->cnum - 1) / 2;

    array = (double *) calloc(num,  sizeof(double));

    biggest = 0.0;
    count = 0;
    for (coord1 = 0; coord1 < cdsA->cnum; ++coord1)
    {
        for (coord2 = coord1 + 1; coord2 < cdsA->cnum; ++coord2)
        {
            for (i = offset; i < cdsA->vlen - offset; ++i)
            {
                for (k = 0; k < fraglen; ++k)
                {
                    tmp = i + k - offset;

                    frag1->x[k] = cdsA->cds[coord1]->x[tmp];
                    frag1->y[k] = cdsA->cds[coord1]->y[tmp];
                    frag1->z[k] = cdsA->cds[coord1]->z[tmp];

                    frag2->x[k] = cdsA->cds[coord2]->x[tmp];
                    frag2->y[k] = cdsA->cds[coord2]->y[tmp];
                    frag2->z[k] = cdsA->cds[coord2]->z[tmp];
                }

                if (center_ca == 1)
                {
                    CenterFragCA(frag1);
                    CenterFragCA(frag2);
                }
                else if (center_ca == 2)
                {
                    CenterFrag(frag1);
                    CenterFrag(frag2);
                }

                var = QuarticHornFrag((const FragCds *) frag1, (const FragCds *) frag2, coeff);

                fprintf(distfile, "%-16.8e \n", var);
                fprintf(distfile2, "%-16.8e \n", sqrt(var));

                if (var > biggest)
                    biggest = var;

                array[count] = sqrt(var);
                ++count;
            }
        }
    }

    FragCdsFree(&frag1);
    FragCdsFree(&frag2);
    free(coeff);
    fclose(distfile);
    fclose(distfile2);
    free(array);

    exit(EXIT_SUCCESS);
}
