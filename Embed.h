/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef LELE_SEEN
#define LELE_SEEN

void
SetupLele(CdsArray *cdsA);

void
CalcLeleCovMat(CdsArray *cdsA);

double
VariancesEDMA(double *var, double **edmamat, double **distmat, const int n);

void
CalcLeleVariances(CdsArray *cdsA);

void
CdsInnerProd(Cds *cds);

void
CdsInnerProd2(Cds *cds);

void
CalcEDMADistMat(CdsArray *cdsA);

void
CalcEDMADistMatNu(CdsArray *cdsA);

void
LeleCenterMat(double **mat, const int len);

void
DoubleCenterMat(double **mat, const int len);

void
CalcMLDistMat(CdsArray *cdsA);

void
CalcMLDistMatNu(CdsArray *cdsA);

void
EmbedAveCds(CdsArray *cdsA);

#endif
