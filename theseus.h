/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef THESEUS_SEEN
#define THESEUS_SEEN


#if __STDC__ != 1
    #error NOT a Standard C environment
#endif


#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <dirent.h>
#include <sys/time.h>
#include <sys/stat.h>
#ifndef __MINGW32__
    #include <sys/resource.h>
#endif
#include <sys/types.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_gamma.h>
#include "Error.h"
#include "CovMat.h"
#include "DLTmath.h"
#include "distfit.h"
#include "Embed.h"
#include "FragDist.h"
#include "HierarchVars.h"
#include "pdbUtils.h"
#include "pdbMalloc.h"
#include "pdbStats.h"
#include "pdbIO.h"
#include "PCAstats.h"
#include "RandCds.h"
#include "MultiPose.h"
#include "MultiPose2MSA.h"
#include "MultiPosePth.h"
#include "QuarticHornFrag.h"
#include "GibbsMet.h"
#include "ProcGSLSVDNu.h"
#include "StructAlign.h"
#include "NWfill.h"
#include "termcol.h"

#endif
