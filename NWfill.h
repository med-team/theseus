/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef NWFILL_SEEN
#define NWFILL_SEEN

#include "StructAlign.h"
#include "pdbMalloc.h"

extern char aa1[];
extern char aa3[];
extern char ss1[];
extern double s_dssp8[8][8];
extern double s_simple[20][20];
extern double s_simpler[20][20];
extern double s_hsdm[20][20];
extern double s_blosum30[20][20];
extern double s_blosum35[20][20];
extern double s_blosum40[20][20];
extern double s_blosum50[20][20];
extern double s_blosum62[20][20];
extern double s_blosum100[20][20];


void
NWfill(NWtable *nw_table, Cds *cds1, Cds *cds2, double *var);

void
ProcrustesFill(NWtable *nw_table, Cds *cds1, Cds *cds2);

void
SuperDistFill(NWtable *nw_table, Cds *cds1, Cds *cds2, double *var);

void
SimpleChargeFill(NWtable *nw_table, Cds *cds1, Cds *cds2);

void
ContactOrderFill(NWtable *nw_table, Cds *cds1, Cds *cds2);

void
BlosumFill(NWtable *nw_table, Cds *cds1, Cds *cds2);

void
DsspFill(NWtable *nw_table);

void
AnglesFill(NWtable *nw_table, const PDBCds *pdbc1, const PDBCds *pdbc2);

double
Dihedral(const double *atm1, const double *atm2,
         const double *atm3, const double *atm4);

void
LogOddsScoresCO(double **mat, int nx, int ny, int convert);

void
MLScores(NWtable *nw_table);

void
MLScoresCO(double **mat, int nx, int ny);

void
LogOddsScores(NWtable *nw_table);

void
BayesScoresCO(double **mat, int nx, int ny);

void
BayesScores(NWtable *nw_table);

#endif
