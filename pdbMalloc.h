/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef PDBMALLOC_SEEN
#define PDBMALLOC_SEEN

#include "Cds.h"
#include "PDBCds.h"


Seq2PDB
*Seq2pdbInit(void);

void
Seq2pdbAlloc(Seq2PDB *seq2pdb, const int seqnum);

void
Seq2pdbDestroy(Seq2PDB **seq2pdb_ptr);

void
Seq2pdbDealloc(Seq2PDB **seq2pdb_ptr);

Algorithm
*AlgorithmInit(void);

void
AlgorithmDestroy(Algorithm **algo);

Statistics
*StatsInit(void);

CdsArray
*CdsArrayInit(void);

void
CdsArrayAllocNum(CdsArray *cdsA, const int cnum);

void
CdsArrayAllocLen(CdsArray *cdsA, const int vlen);

void
CdsArrayAlloc(CdsArray *cdsA, const int cnum, const int vlen);

void
CdsArraySetup(CdsArray *cdsA);

void
DistMatsAlloc(CdsArray *cdsA);

void
DistMatsDestroy(CdsArray *cdsA);

void
CovMatsDestroy(CdsArray *cdsA);

void
PCAAlloc(CdsArray *cdsA);

void
PCADestroy(CdsArray *cdsA);

void
CdsArrayDestroy(CdsArray **cdsA_ptr);

Cds
*CdsInit(void);

void
CdsAlloc(Cds *cds, const int vlen);

void
CdsSetup(Cds *cds);

void
CdsDestroy(Cds **cds_ptr);

PDBCdsArray
*PDBCdsArrayInit(void);

void
PDBCdsArrayAllocNum(PDBCdsArray *pdbA, const int cnum);

void
PDBCdsArrayAllocLen(PDBCdsArray *pdbA, const int vlen);

void
PDBCdsArrayAlloc(PDBCdsArray *pdbA, const int cnum, const int vlen);

void
PDBCdsArrayDestroy(PDBCdsArray **pdbA_ptr);

PDBCds
*PDBCdsInit(void);

void
PDBCdsAlloc(PDBCds *pdbcds, const int vlen);

void
PDBCdsDestroy(PDBCds **cds_ptr);

#endif
