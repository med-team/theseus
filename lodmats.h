/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef LODMATS_SEEN
#define LODMATS_SEEN

extern const char aa1[];
extern const char aa3[];
extern const char aan1[];
extern const char aan3[];
extern const char ss1[];
extern const double aafreq_blosum50[20];
extern const double s_dssp8[8][8];
extern const double s_simple[20][20];
extern const double s_simpler[20][20];
extern const double s_hsdm[20][20];
extern const double s_blosum30[20][20];
extern const double s_blosum35[20][20];
extern const double s_blosum40[20][20];
extern const double s_blosum50[20][20];
extern const double s_blosum62[20][20];
extern const double s_blosum100[20][20];

#endif
