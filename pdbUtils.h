/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef PDBUTILS_SEEN
#define PDBUTILS_SEEN

#include <pthread.h>
#include "pdbMalloc.h"
#include "Threads.h"

void
CdsCopyXYZ(Cds *cds1, const Cds *cds2);

void
CdsCopyAll(Cds *cds1, const Cds *cds2);

void
CdsArrayCopy(CdsArray *cdsA1, const CdsArray *cdsA2);

void
CdsCopy(Cds *cds1, const Cds *cds2);

void
PDBCdsCopyAll(PDBCds *cds1, const PDBCds *cds2);

void
MatMultCdsMultMatDiag(Cds *outcds, const double **matK, const Cds *cds);

void
MatDiagMultCdsMultMatDiag(Cds *outcds, const double *wtK, const Cds *cds);

void
CopyCds2PDB(PDBCds *pdbcds, const Cds *cds);

int
NMRValidPDBCdsArray(PDBCdsArray *pdbA);

int
NMRCheckPDBCdsArray(PDBCdsArray *pdbA);

void
TransformPDBCdsIp(PDBCds *pdbcds);

void
RotateCdsIp(Cds *cds, const double **rmat);

void
RotateCdsIp2(double **c1, const int vlen, const double **rmat);

void
RotateCdsArrayIp(CdsArray *cdsA, const double **rmat);

void
RotateCdsOp(double **c2, const double **c1, const double **rmat, const int vlen);

void
TransformCdsIp(Cds *cds);

void
ScaleCds(Cds *cds, const double scale);

void
CenMass(Cds *cds);

void
CenMassWtOp(Cds *cds, const CdsArray *weights);

void
CenMass2(const double **cds, const int vlen, double *center);

void
CenMassWt2(const double **cds, const double *wts, const int vlen, double *center);

void
CenMassWtIp(Cds *cds, const double *weights);

void
CenMassCovOp(Cds *cds, const CdsArray *weights);

void
CalcCovCds(Cds *cds, const double **wtmat);

void
CenMassCov(Cds *cds, const double **wtmat);

void
CenMassCov2(const double **c, double **cc, const double **wtmat, const int vlen, double *center);

void
CenMassCov(Cds *cds, const double **wtmat);

void
CenMassWt(Cds *cds);

void
CenMassNuVec(const double **c, const int *nu, double *cenmass, const int vlen);

void
CenMassWtNu2(const double **cds, const double **ave, const int *nu, const double *wts, const int vlen, const double **rmat, double *center);

void
CenMassWtIpNu(Cds *cds, const double *wts);

void
CenMassWtIpEM(Cds *cds, const Cds *avecds, const double *wts);

void
ApplyCenter(Cds *cds, const double cenx, const double ceny, const double cenz);

void
ApplyNegCenterIp(Cds *cds);

void
ApplyCenterIp(Cds *cds);

void
ApplyCenterOp(Cds *cds1, const Cds *cds2);

void
TranslateCdsOp2(double **cds2, const double **cds1, const int vlen, const double *center);

void
TransCdsIp(double **c, const double *trans, const int vlen);

void
NegTransCdsIp(double **c, const double *trans, const int vlen);

void
EM_MissingCds(CdsArray *cdsA);

void
AveCds_pth(CdsArray *cdsA, AveData **avedata, pthread_t *callThd,
              pthread_attr_t *attr, const int thrdnum);

double
HierAveCds(CdsArray *cdsA);

void
AveCds(CdsArray *cdsA);

void
AveCdsTB(CdsArray *cdsA, int omit);

void
AveCdsNu(CdsArray *cdsA);

void
CalcCdsPrincAxes(Cds *cds, double **r, double **u, double **vt, double *lambda, const double *wts);

void
SumCdsTB(CdsArray *cdsA, const int exclude);

#endif
