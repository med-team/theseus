/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef TERMCOL_SEEN
#define TERMCOL_SEEN

/* regular */
extern const char tc_black[];
extern const char tc_red[];
extern const char tc_green[];
extern const char tc_yellow[];
extern const char tc_blue[];
extern const char tc_purple[];
extern const char tc_cyan[];
extern const char tc_white[];

/* bold (xterm) or light (console) */
extern const char tc_BLACK[];
extern const char tc_RED[];
extern const char tc_GREEN[];
extern const char tc_YELLOW[];
extern const char tc_BLUE[];
extern const char tc_PURPLE[];
extern const char tc_CYAN[];
extern const char tc_WHITE[];

/* underline */
extern const char tc_ublack[];
extern const char tc_ured[];
extern const char tc_ugreen[];
extern const char tc_uyellow[];
extern const char tc_ublue[];
extern const char tc_upurple[];
extern const char tc_ucyan[];
extern const char tc_uwhite[];

/* blink */
extern const char tc_bblack[];
extern const char tc_bred[];
extern const char tc_bgreen[];
extern const char tc_byellow[];
extern const char tc_bblue[];
extern const char tc_bpurple[];
extern const char tc_bcyan[];
extern const char tc_bwhite[];

/* inverse */
extern const char tc_iblack[];
extern const char tc_ired[];
extern const char tc_igreen[];
extern const char tc_iyellow[];
extern const char tc_iblue[];
extern const char tc_ipurple[];
extern const char tc_icyan[];
extern const char tc_iwhite[];

/* concealed */
extern const char tc_cblack[];
extern const char tc_cred[];
extern const char tc_cgreen[];
extern const char tc_cyellow[];
extern const char tc_cblue[];
extern const char tc_cpurple[];
extern const char tc_ccyan[];
extern const char tc_cwhite[];

extern const char tc_NC[];

#endif
