/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef COVMAT_SEEN
#define COVMAT_SEEN

#include "pdbMalloc.h"

void
PrintCovMatGnuPlot(const double **mat, const int dim, char *outfile);

void
SetupCovWeighting(CdsArray *cdsA);

double
ConditionNumber(const double *v, const int n);

int
CheckZeroVariances(CdsArray *cdsA);

void
CalcBfactC(CdsArray *cdsA);

void
CalcCovariances(CdsArray *cdsA);

void
CalcCovMat(CdsArray *cdsA);

void
CalcCovMatNu(CdsArray *cdsA);

void
CalcFullCovMat(CdsArray *cdsA);

void
CovMat2CorMat(double **CovMat, const int size);

void
CorMat2CovMat(double **CovMat, const double *vars, const int size);

void
PrintCovMat(CdsArray *cdsA);

#endif
