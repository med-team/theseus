/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef RANDCOORDS_SEEN
#define RANDCOORDS_SEEN

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

void
rand_rotation(const double *x, double **M);

void
rand_translation(double *x, double var, const gsl_rng *r2);

void
RandRotCdsArray(CdsArray *cdsA, const gsl_rng *r2);

void
RandTransCdsArray(CdsArray *cdsA, const double var, const gsl_rng *r2);

double
thirdOpoly_dev(const double b, const double c, const double d, const gsl_rng *r2);

int
quadratic_solver(const double a, const double b, const double c,
                 double *x1, double *x2);

void
RandCds(CdsArray *cdsA, const gsl_rng *r2);

void
RandCds_2sdf(CdsArray *cdsA, gsl_rng *r2);

double
RandScale(double variance, int randmeth, double a, double b);

PDBCdsArray
*GetRandPDBCds(char *pdbfile_name, int cnum, int vlen0);

PDBCdsArray
*MakeRandPDBCds(int cnum, int vlen, double *radii, const gsl_rng *r2);

void
RandUsage(void);

#endif
