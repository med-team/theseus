/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef DSSPLITE_SEEN
#define DSSPLITE_SEEN

typedef double Vector[3];

typedef struct DSSP_
{
    /* PDB file vectors */
    double         *x, *y, *z;
    char          **resName, **name;
    int            *resSeq;

    /* DSSP vectors */
    int             len;
    double        **C, **O, **N, **H, **CA;
    char          **residue; /* name of residue */
    int            *Hbond;
    int           **bondatm;
    int           **bridgeP, **bridgeA;
    int            *bridgePn, *bridgeAn;
    int            *bend;
    int            *turn5, *turn4, *turn3;
    int            *cys, *disulph;
    char           *summary;
} DSSP;


char
*dssplite(double *x, double *y, double *z,
          char **resName, int *resSeq, char **name,
          int len);

char
*dssplite_interleave(double *coords,
                     char **resName, int *resSeq, char **name,
                     int len);

char
*dssplite_mat(double **coords,
              char **resName, int *resSeq, char **name,
              int len);

int
TestHBond(DSSP *dssp, int hb1, int hb2);

void
FlagBends(DSSP *dssp);

void
FlagTurns(DSSP *dssp);

void
Find5Helices(DSSP *dssp);

void
Find3Helices(DSSP *dssp);

void
FlagBridges(DSSP *dssp);

void
FindStrands(DSSP *dssp);

int
isbridgeP(DSSP *dssp, int atom);

int
isbridgeA(DSSP *dssp, int atom);

int
eqbridgeP(DSSP *dssp, int atom1, int atom2);

int
eqbridgeA(DSSP *dssp, int atom1, int atom2);

void
Find4Helices(DSSP *dssp);

int
FlagHBonds(DSSP *dssp);

void
GetCONHCA(DSSP *dssp);

double
Distance(double **atom1, int atmid1, double **atom2, int atmid2);

double
DistanceSqr(double **atom1, int atmid1, double **atom2, int atmid2);

double
Dihedralangle(double **Ad, int An,
              double **Bd, int Bn,
              double **Cd, int Cn,
              double **Dd, int Dn);

int
HbondEnergy(DSSP *dssp, int co, int nh);

DSSP
*DSSPinit(void);

void
DSSPalloc(DSSP *dssp, int len);

void
DSSPfree(DSSP **dssp);

void
PrintCONHCA(DSSP *dssp);

void
PrintSummary(DSSP *dssp);

#endif
