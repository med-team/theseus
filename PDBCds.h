/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef PDBCOORDS_SEEN
#define PDBCOORDS_SEEN

#include "msa.h"
#include "Cds.h"


typedef struct Seq2PDB Seq2PDB;
typedef struct PDBCds PDBCds;
typedef struct PDBCdsArray PDBCdsArray;


/* A structure for mapping sequence names in an alignment to their corresponding
   pdbfiles */
struct Seq2PDB
{
    int             seqnum;
    char          **pdbfile_name;
    char          **seqname;

    char           *pdbfile_name_space;
    char           *seqname_space;

    int            *map;
    MSA            *msa;
    int            *singletons;
};


/* PDBCds is mostly for reading/writing PDB files */
struct PDBCds
{
    char            filename[FILENAME_MAX];
    int             model;

    int             vlen; /* number of coordinates */

    double        **matrix;
    double         *translation;
    double          scale;

    /* PDB ATOM/HETATM fields */
    char          **record; /* ATOM or HETATM */
    unsigned int   *serial; /* atom number */
    char           *Hnum;
    char          **name; /* atom name, e.g. CA */
    char           *altLoc; /* alternate location identifier, usu A, B, or C */
    char          **resName; /* residue name, e.g. ALA, TYR, PHE, etc. */
    char           *xchainID;
    char           *chainID;
    int            *resSeq; /* residue number */
    char           *iCode;
    double         *x; /* x,y,z atomic coordinates */
    double         *y;
    double         *z;
    double         *occupancy;
    double         *tempFactor; /* B-factor */
    char          **segID;
    char          **element;
    char          **charge;

    int            *nu;
    double        **c; /* coords matrix, xyz above aliased to it */

    /* not to be accessed - for space only */
    char           *record_space;
    char           *name_space;
    char           *resName_space;
    char           *segID_space;
    char           *element_space;
    char           *charge_space;
};


struct PDBCdsArray
{
    PDBCds            **cds;        /* pointer to an array of cnum pointers to Cds */
    PDBCds             *avecds;     /* average Cds of all in CdsArray */
    struct CdsArray    *cdsA;       /* associated CdsArray - do not free */
    struct CdsArray    *scratchA;   /* do not free */
    int                 vlen;       /* number of coordinates */
    int                 cnum;       /* number of Cds in array */
    int                *upper, *lower;
    int                 range_num;
    Seq2PDB            *seq2pdb;
};

#endif
