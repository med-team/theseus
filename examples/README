This directory contains files for demonstrating how THESEUS can be run.

********************************************************************************
The pdb file 1s40.pdb is an NMR family of a telomere protein bound to a ssDNA.
It has 10 models. To do the ML superposition, run the following command:

theseus 1s40.pdb


********************************************************************************
In this directory there are also multiple cytochrome c pdb files. These have
different amino acid sequences, yet similar structures. These can be
superimposed using either of the following three commands.

theseus -A cytc.aln -M cytc.filemap d*.pdb

theseus -A cytc.aln d*.pdb

or

theseus_align -f d*.pdb

The first command requires three files, a sequence alignment file, a mapfile
that correlates each sequence name in the alignment with a pdb file, and
a pdb file. See the man page or the main README for more details.

The second command is similar to the first, but it assumes that the sequences
in the sequence alignment file have names that correspond *exactly* to their
pdb filenames. (Then the mapfile is unneeded.)

The third command is easiest to use, but it requires that you have a sequence
alignment program (such as MUSCLE or CLUSTALW) installed and available for the
script to call. You must modify the first few lines of the script so that it
knows where to find the executable binaries for THESEUS and the multiple
sequence alignment program you wish to use. Furthermore, the sequence alignment
program must be able to output clustal or fasta formatted files.

