/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef PDBSTATS_SEEN
#define PDBSTATS_SEEN

#include "pdbMalloc.h"

void
CheckVars(CdsArray *cdsA);

void
CalcDf(CdsArray *cdsA);

void
WriteVariance(CdsArray *cdsA, char *outfile_name);

void
WriteTransformations(CdsArray *cdsA, char *outfile_name);

void
WriteResiduals(CdsArray *cdsA, char *outfile_name);

void
Bfacts2PrVars(CdsArray *cdsA, int coord);

double
CalcPRMSD(CdsArray *cdsA);

double
CalcMLRMSD(CdsArray *cdsA);

double
SqrCdsDist(const Cds *cds1, const int atom1,
           const Cds *cds2, const int atom2);

double
SqrCdsDistMahal2(const Cds *cds1, const int atom1,
                 const Cds *cds2, const int atom2,
                 const double weight);

void
CalcNormResiduals(CdsArray *cdsA);

void
CalcNormResidualsLS(CdsArray *cdsA);

double
FrobTerm(CdsArray *cdsA);

double
CalcMgLogLCov(CdsArray *cdsA);

double
CalcMgLogL(CdsArray *cdsA);

double
CalcMgLogLNu(CdsArray *cdsA);

double
CalcParamNum(CdsArray *cdsA);

void
CalcSampleVar(CdsArray *cdsA);

void
CalcSampleVarNu(CdsArray *cdsA);

double
TrCdsInnerProd(Cds *cds, const int len);

double
TrCdsInnerProdWt(Cds *cds, const int len, const double *w);

double
TrCdsInnerProd2(Cds *cds1, Cds *cds2, const int len);

double
TrCdsInnerProdWt2(Cds *cds1, Cds *cds2, const int len, const double *w);

void
UnbiasMean(CdsArray *scratchA);

void
CalcStats(CdsArray *incdsA);

void
CalcPreStats(CdsArray *cdsA);

#endif
