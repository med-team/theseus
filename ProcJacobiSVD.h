/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef PROCJACOBISVD_SEEN
#define PROCJACOBISVD_SEEN

#include "FragCds.h"

double
ProcJacobiSVD(const Cds *cds1, const Cds *cds2, double **rotmat,
              const double *weights,
              double **Rmat, double **Umat, double **VTmat, double *sigma);

double
ProcJacobiSVDvan(const Cds *cds1, const Cds *cds2, double **rotmat,
                 double **Rmat, double **Umat, double **VTmat, double *sigma);

double
ProcJacobiSVDCov(Cds *cds1, Cds *cds2, double **rotmat,
                 const double **covmat,
                 double **Rmat, double **Umat, double **VTmat,
                 double *sigma);

double
ProcJacobiSVDFrag(const FragCds *cds1, const FragCds *cds2, double **rotmat,
                  double **Rmat, double **Umat, double **VTmat, double *sigma);

#endif
