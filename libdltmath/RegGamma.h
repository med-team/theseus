/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef REGGAMMA_SEEN
#define REGGAMMA_SEEN

/* double */
/* IncompleteGamma (double theA, double theX); */
/*  */
/* double */
/* regularizedGammaP(double a, double x, double epsilon, int maxIterations); */
/*  */
/* double */
/* regularizedGammaQ(double a, double x, double epsilon, int maxIterations); */
/*  */
/* double */
/* gamain( double x, double p, double g ); */
/*  */
/* double */
/* gamln( double x ); */
/*  */
/* void */
/* grat1(double a, double x, double r, double *p, double *q, */
/*           double eps); */

double
InBeta(double a, double b, double x);

double
InGamma(double a, double x);

double
InGammaQ(double a, double x);

double
InGammaP(double a, double x);

#endif
