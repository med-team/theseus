/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef MAT3UTILS_SEEN
#define MAT3UTILS_SEEN

void
Mat3Print(double **matrix);

double
**Mat3Ident(double **matrix);

int
Mat3Eq(const double **matrix1, const double **matrix2, const double precision);

double
Mat3FrobDiff(const double **matrix1, const double **matrix2);

int
Mat3FrobEq(const double **matrix1, const double **matrix2, const double precision);

void
Mat3Cpy(double **matrix2, const double **matrix1);

void
Mat3MultOp(double **C, const double **A, const double **B);

void
Mat3MultIp(double **A, const double **B);

void
Mat3MultUSVOp(double **C, const double **U, double *S, const double **V);

void
Mat3PreMultIp(const double **A, double **B);

void
Mat3Sqr(double **C, const double **A);

void
Mat3SqrTrans2(double **C, const double **A);

void
Mat3SqrTrans1(double **C, const double **A);

void
Mat3TransSqr(double **C, const double **A);

void
Mat3MultTransA(double **C, const double **A, const double **B);

void
Mat3MultTransB(double **C, const double **A, const double **B);

void
Mat3Add(double **C, const double **A, const double **B);

void
Mat3Sub(double **A, double **B, double **C);

void
Mat3TransposeIp(double **matrix);

void
Mat3TransposeOp(double **matrix2, const double **matrix1);

double
Mat3Det(const double **matrix);

void
Mat3Invert(double **outmat, const double **inmat);

void
Mat3SymInvert(double **outmat, const double **inmat);

void
Mat3MultVec(double *outv, const double **inmat, const double *vec);

int
VerifyRotMat(double **rotmat, double tol);

void
ClosestRotMatIp(double **inmat);

double
RotMat2AxisAngle(double **rot, double *v);

double
RotMat2AxisAngleQuat(double **rot, double *v);

#endif
