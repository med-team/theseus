/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef VECUTILS_SEEN
#define VECUTILS_SEEN

void
VecPrint(double *vec, const int size);

void
RotVec(double *newvec, double *vec, double **rotmat);

void
InvRotVec(double *newvec, double *vec, double **rotmat);

void
RotVecAdd(double *newvec, double *vec, double **rotmat);

void
InvRotVecAdd(double *newvec, double *vec, double **rotmat);

int
VecEq(const double *vec1, const double *vec2, const int len, const double tol);

void
RevVecIp(double *vec, const int len);

double
VecSmallest(double *vec, const int len);

double
VecBiggest(double *vec, const int len);

#endif /* !MATRIXUTILS_SEEN */ 
