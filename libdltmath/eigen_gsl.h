/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/
#ifndef EIGEN_GSL_SEEN
#define EIGEN_GSL_SEEN

void
EigenvalsGSL(const double **mat, const int dim, double *eval);

void
EigenvalsGSLDest(double **mat, const int dim, double *eval);

void
EigenGSL(const double **mat, const int dim, double *eval, double **evec, int order);

void
EigenGSLDest(double **mat, const int dim, double *eval, double **evec, int order);

void
CalcGSLSVD3(double **a, double **u, double *s, double **vt);

void
svdGSLDest(double **A, const int dim, double *singval, double **V);

void
svdGSLJacobiDest(double **A, const int dim, double *singval, double **V);

void
CholeskyGSLDest(double **A, const int dim);

void
PseudoinvSymGSL(const double **inmat, double **outmat, int n, double tol);

double
InvSymEigenOp(double **invmat, const double **mat, int n,
              double *evals, double **evecs, const double tol);

void
EigenReconSym(double **mat, const double **evecs, const double *evals, const int n);

#endif
