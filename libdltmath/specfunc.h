/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef SPECFUNC_SEEN
#define SPECFUNC_SEEN

double 
Hermite(const int n, const double x);

double
BesselI(const double nu, const double z);

double
BesselI0(const double z);

double
BesselI1(const double z);

double
bessi(const int n, const double x);

double
bessi0(const double x);

double
bessi1(const double x);

double
UpperIncompleteGamma(const double a, const double x);

double
gammp(const double a, const double x);

double
gammq(const double a, const double x);

double
gcf(double a, double x);

double
gser(double a, double x);

double
IncompleteGamma(const double x, const double alpha);

double
lngamma(const double xx);

double
mygamma(const double xx);

double
harmonic(int x);

double
polygamma(int k, double x);

double
betai(double a, double b, double x);

double
betacf(double a, double b, double x);

double
beta(double z, double w);

int
findmin(const double *vec, const int len);

int
findmax(const double *vec, const int len);

double
mymaxdbl(const double x, const double y);

double
mymindbl(const double x, const double y);

int
mymaxint(const int x, const int y);

int
myminint(const int x, const int y);

int
myround(const double num);

double
mysquare(const double val);

double
mycube(const double val);

double
mypow4(double val);

#endif
