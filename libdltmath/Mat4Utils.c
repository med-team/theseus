/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "Mat4Utils.h"


void
Mat4Print(double **matrix)
{
    int             i;

    printf("\n");
    for (i = 0; i < 4; ++i)
    {
        printf(" [ % 14.8f % 14.8f % 14.8f % 14.8f ]\n",
               matrix[i][0],
               matrix[i][1],
               matrix[i][2],
               matrix[i][3]);
    }
    printf("\n");

    fflush(NULL);
}


void
Mat4Copy(double **matrix2, const double **matrix1)
{
    int            i;

    for (i = 0; i < 4; ++i)
       memcpy(matrix2[i], matrix1[i], 4 * sizeof(double));
}


void
Mat4TransposeIp(double **matrix)
{
    double tmp;

    tmp          = matrix[0][1];
    matrix[0][1] = matrix[1][0];
    matrix[1][0] = tmp;

    tmp          = matrix[0][2];
    matrix[0][2] = matrix[2][0];
    matrix[2][0] = tmp;

    tmp          = matrix[0][3];
    matrix[0][3] = matrix[3][0];
    matrix[3][0] = tmp;

    tmp          = matrix[2][1];
    matrix[2][1] = matrix[1][2];
    matrix[1][2] = tmp;

    tmp          = matrix[3][1];
    matrix[3][1] = matrix[1][3];
    matrix[1][3] = tmp;

    tmp          = matrix[3][2];
    matrix[3][2] = matrix[2][3];
    matrix[2][3] = tmp;
}


void
Mat4TransposeOp(double **matrix2, const double **matrix1)
{
    matrix2[0][0] = matrix1[0][0];
    matrix2[0][1] = matrix1[1][0];
    matrix2[0][2] = matrix1[2][0];
    matrix2[0][3] = matrix1[3][0];

    matrix2[1][0] = matrix1[0][1];
    matrix2[1][1] = matrix1[1][1];
    matrix2[1][2] = matrix1[2][1];
    matrix2[1][3] = matrix1[3][1];
    
    matrix2[2][0] = matrix1[0][2];
    matrix2[2][1] = matrix1[1][2];
    matrix2[2][2] = matrix1[2][2];
    matrix2[2][3] = matrix1[3][2];

    matrix2[3][0] = matrix1[0][3];
    matrix2[3][1] = matrix1[1][3];
    matrix2[3][2] = matrix1[2][3];
    matrix2[3][3] = matrix1[3][3];
}
