/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef QUICKSORT_SEEN
#define QUICKSORT_SEEN

/*---------------       quicksort.h              --------------*/
/*
 * The key TYPE.
 * COARRAY_T is the type of the companion array
 * The keys are the array items moved with the SWAP macro
 * around using the SWAP macro.
 * the comparison macros can compare either the key or things
 * referenced by the key (if its a pointer)
 */ 
typedef double      KEY_T;
typedef char       *COARRAY_T;
/*
 * The comparison macros:
 *
 *  GT(x, y)  as   (strcmp((x),(y)) > 0)
 *  LT(x, y)  as   (strcmp((x),(y)) < 0)
 *  GE(x, y)  as   (strcmp((x),(y)) >= 0)
 *  LE(x, y)  as   (strcmp((x),(y)) <= 0)
 *  EQ(x, y)  as   (strcmp((x),(y)) == 0)
 *  NE(x, y)  as   (strcmp((x),(y)) != 0)
 */
#define GT(x, y) ((x) > (y))
#define LT(x, y) ((x) < (y))
#define GE(x, y) ((x) >= (y))
#define LE(x, y) ((x) <= (y))
#define EQ(x, y) ((x) == (y))
#define NE(x, y) ((x) != (y))

/*
 * Swap macro:
 */
 
/* double              tempd; */
/* char               *tempc; */
/*  */
/* #define SWAPD(x, y) tempd = (x); (x) = (y); (y) = tempd */
/* #define SWAPC(x, y) tempc = (x); (x) = (y); (y) = tempc */

extern void
swapd(double *x, double *y);

extern void
swapc(char **x, char **y);

extern void
insort2 (KEY_T *array1, COARRAY_T *array2, int len);

extern void
insort2d (KEY_T *array1, KEY_T *array2, int len);

extern void
insort (KEY_T *array1, int len);

extern void
partial_quicksort2 (KEY_T *array1, COARRAY_T *array2, int lower, int upper);

extern void
partial_quicksort2d (KEY_T *array1, KEY_T *array2, int lower, int upper);

extern void
partial_quicksort (KEY_T *array, int lower, int upper);

extern void
quicksort2 (KEY_T *array1, COARRAY_T *array2, int len);

extern void
quicksort2d (KEY_T *array1, KEY_T *array2, int len);

extern void
quicksort (KEY_T *array, int len);

#endif
