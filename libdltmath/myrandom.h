/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef MYRANDOM_SEEN
#define MYRANDOM_SEEN

void
init_genrand(unsigned long s);

void
init_by_array(unsigned long init_key[], unsigned long key_length);

unsigned long
genrand_int32(void);

long
genrand_int31(void);

double
genrand_real1(void);

double
genrand_real2(void);

double
genrand_real3(void);

double
genrand_res53(void);

double
expondev(void);

double
gaussdev(void);

double
Normal(void);

void
shuffle(int *a, int n);

void
shufflef(double *a, int n);

#endif
