/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/


#include <math.h>
#include <float.h>
#include <stdio.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_math.h>


double
MultivarLnGamma(const int p, const double a)
{
    int            j;
    double         mvlng, x;

    mvlng = 0.0;
    for (j = 1; j <= p; ++j)
    {
        x = a + (1.0-j)*0.5;
        //printf("MVLNGAMMA: %d %d %g %g\n", j, p, a, x); fflush(NULL);
        mvlng += gsl_sf_lngamma(x);
    }

    mvlng += 0.25 * p * (p - 1.0) * log(M_PI);

    return(mvlng);
}
