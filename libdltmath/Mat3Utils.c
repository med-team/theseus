/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include "DLTmath.h"
#include "MatUtils.h"
#include "Mat3Utils.h"


void
Mat3Print(double **matrix)
{
    int             i;

    printf("\n");
    for (i = 0; i < 3; ++i)
    {
        printf(" [ % 14.8f % 14.8f % 14.8f ]\n",
               matrix[i][0],
               matrix[i][1],
               matrix[i][2]);
    }

    fflush(NULL);
}


double
**Mat3Ident(double **matrix)
{
    matrix[0][0] = matrix[1][1] = matrix[2][2] = 1.0;
    matrix[0][1] = matrix[0][2] = matrix[1][0] =
    matrix[1][2] = matrix[2][0] = matrix[2][1] = 0.0;

    return(matrix);
}


int
Mat3Eq(const double **matrix1, const double **matrix2, const double precision)
{
    if(fabs(matrix2[0][0] - matrix1[0][0]) < precision &&
       fabs(matrix2[0][1] - matrix1[0][1]) < precision &&
       fabs(matrix2[0][2] - matrix1[0][2]) < precision &&

       fabs(matrix2[1][0] - matrix1[1][0]) < precision &&
       fabs(matrix2[1][1] - matrix1[1][1]) < precision &&
       fabs(matrix2[1][2] - matrix1[1][2]) < precision &&

       fabs(matrix2[2][0] - matrix1[2][0]) < precision &&
       fabs(matrix2[2][1] - matrix1[2][1]) < precision &&
       fabs(matrix2[2][2] - matrix1[2][2]) < precision)
        return(1);
    else
        return(0);
}


double
Mat3FrobDiff(const double **mat1, const double **mat2)
{
    int             i, j;
    double          frobnorm, tmp;

    frobnorm = 0.0;
    for (i = 0; i < 3; ++i)
    {
        for (j = 0; j < 3; ++j)
        {
            tmp = mat2[i][j] - mat1[i][j];
            frobnorm += tmp * tmp;
        }
    }

    return(sqrt(frobnorm / 3.0));
}


/* check for the equivalence of two matrices based on the Frobenius
   norm criterion (more statistically justified than the above) */
int
Mat3FrobEq(const double **mat1, const double **mat2, const double precision)
{
    int             i, j;
    double          frobnorm, tmp;

    frobnorm = 0.0;
    for (i = 0; i < 3; ++i)
    {
        for (j = 0; j < 3; ++j)
        {
            tmp = mat2[i][j] - mat1[i][j];
            frobnorm += tmp * tmp;
        }
    }

    if (sqrt(frobnorm / 3.0) < precision)
        return(1);
    else
        return(0);
}


void
Mat3Cpy(double **matrix2, const double **matrix1)
{
    memcpy(&matrix2[0][0], &matrix1[0][0], 9 * sizeof(double));
}


/* A x B = C */
void
Mat3MultOp(double **C, const double **A, const double **B)
{
    C[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0] + A[0][2]*B[2][0];
    C[1][0] = A[1][0]*B[0][0] + A[1][1]*B[1][0] + A[1][2]*B[2][0];
    C[2][0] = A[2][0]*B[0][0] + A[2][1]*B[1][0] + A[2][2]*B[2][0];

    C[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1] + A[0][2]*B[2][1];
    C[1][1] = A[1][0]*B[0][1] + A[1][1]*B[1][1] + A[1][2]*B[2][1];
    C[2][1] = A[2][0]*B[0][1] + A[2][1]*B[1][1] + A[2][2]*B[2][1];

    C[0][2] = A[0][0]*B[0][2] + A[0][1]*B[1][2] + A[0][2]*B[2][2];
    C[1][2] = A[1][0]*B[0][2] + A[1][1]*B[1][2] + A[1][2]*B[2][2];
    C[2][2] = A[2][0]*B[0][2] + A[2][1]*B[1][2] + A[2][2]*B[2][2];
}


/* A = A B */
void
Mat3MultIp(double **A, const double **B)
{
    double C00, C10, C20, C01, C11, C21, C02, C12, C22;

    C00 = A[0][0]*B[0][0] + A[0][1]*B[1][0] + A[0][2]*B[2][0];
    C10 = A[1][0]*B[0][0] + A[1][1]*B[1][0] + A[1][2]*B[2][0];
    C20 = A[2][0]*B[0][0] + A[2][1]*B[1][0] + A[2][2]*B[2][0];

    C01 = A[0][0]*B[0][1] + A[0][1]*B[1][1] + A[0][2]*B[2][1];
    C11 = A[1][0]*B[0][1] + A[1][1]*B[1][1] + A[1][2]*B[2][1];
    C21 = A[2][0]*B[0][1] + A[2][1]*B[1][1] + A[2][2]*B[2][1];

    C02 = A[0][0]*B[0][2] + A[0][1]*B[1][2] + A[0][2]*B[2][2];
    C12 = A[1][0]*B[0][2] + A[1][1]*B[1][2] + A[1][2]*B[2][2];
    C22 = A[2][0]*B[0][2] + A[2][1]*B[1][2] + A[2][2]*B[2][2];

    A[0][0] = C00;
    A[1][0] = C10;
    A[2][0] = C20;

    A[0][1] = C01;
    A[1][1] = C11;
    A[2][1] = C21;

    A[0][2] = C02;
    A[1][2] = C12;
    A[2][2] = C22;
}


/* C = USV, where S is diagonal */
void
Mat3MultUSVOp(double **C, const double **U, double *S, const double **V)
{
    C[0][0] = (U[0][0] * S[0]) * V[0][0] + (U[0][1] * S[1]) * V[1][0] + (U[0][2] * S[2]) * V[2][0];
    C[1][0] = (U[1][0] * S[0]) * V[0][0] + (U[1][1] * S[1]) * V[1][0] + (U[1][2] * S[2]) * V[2][0];
    C[2][0] = (U[2][0] * S[0]) * V[0][0] + (U[2][1] * S[1]) * V[1][0] + (U[2][2] * S[2]) * V[2][0];

    C[0][1] = (U[0][0] * S[0]) * V[0][1] + (U[0][1] * S[1]) * V[1][1] + (U[0][2] * S[2]) * V[2][1];
    C[1][1] = (U[1][0] * S[0]) * V[0][1] + (U[1][1] * S[1]) * V[1][1] + (U[1][2] * S[2]) * V[2][1];
    C[2][1] = (U[2][0] * S[0]) * V[0][1] + (U[2][1] * S[1]) * V[1][1] + (U[2][2] * S[2]) * V[2][1];

    C[0][2] = (U[0][0] * S[0]) * V[0][2] + (U[0][1] * S[1]) * V[1][2] + (U[0][2] * S[2]) * V[2][2];
    C[1][2] = (U[1][0] * S[0]) * V[0][2] + (U[1][1] * S[1]) * V[1][2] + (U[1][2] * S[2]) * V[2][2];
    C[2][2] = (U[2][0] * S[0]) * V[0][2] + (U[2][1] * S[1]) * V[1][2] + (U[2][2] * S[2]) * V[2][2];
}


/* B = A B */
void
Mat3PreMultIp(const double **A, double **B)
{
    double C00, C10, C20, C01, C11, C21, C02, C12, C22;

    C00 = B[0][0]*A[0][0] + B[0][1]*A[1][0] + B[0][2]*A[2][0];
    C10 = B[1][0]*A[0][0] + B[1][1]*A[1][0] + B[1][2]*A[2][0];
    C20 = B[2][0]*A[0][0] + B[2][1]*A[1][0] + B[2][2]*A[2][0];

    C01 = B[0][0]*A[0][1] + B[0][1]*A[1][1] + B[0][2]*A[2][1];
    C11 = B[1][0]*A[0][1] + B[1][1]*A[1][1] + B[1][2]*A[2][1];
    C21 = B[2][0]*A[0][1] + B[2][1]*A[1][1] + B[2][2]*A[2][1];

    C02 = B[0][0]*A[0][2] + B[0][1]*A[1][2] + B[0][2]*A[2][2];
    C12 = B[1][0]*A[0][2] + B[1][1]*A[1][2] + B[1][2]*A[2][2];
    C22 = B[2][0]*A[0][2] + B[2][1]*A[1][2] + B[2][2]*A[2][2];

    B[0][0] = C00;
    B[1][0] = C10;
    B[2][0] = C20;

    B[0][1] = C01;
    B[1][1] = C11;
    B[2][1] = C21;

    B[0][2] = C02;
    B[1][2] = C12;
    B[2][2] = C22;
}


/* A x A = C */
void
Mat3Sqr(double **C, const double **A)
{
    C[0][0] = A[0][0]*A[0][0] + A[0][1]*A[1][0] + A[0][2]*A[2][0];
    C[1][0] = A[1][0]*A[0][0] + A[1][1]*A[1][0] + A[1][2]*A[2][0];
    C[2][0] = A[2][0]*A[0][0] + A[2][1]*A[1][0] + A[2][2]*A[2][0];

    C[0][1] = A[0][0]*A[0][1] + A[0][1]*A[1][1] + A[0][2]*A[2][1];
    C[1][1] = A[1][0]*A[0][1] + A[1][1]*A[1][1] + A[1][2]*A[2][1];
    C[2][1] = A[2][0]*A[0][1] + A[2][1]*A[1][1] + A[2][2]*A[2][1];

    C[0][2] = A[0][0]*A[0][2] + A[0][1]*A[1][2] + A[0][2]*A[2][2];
    C[1][2] = A[1][0]*A[0][2] + A[1][1]*A[1][2] + A[1][2]*A[2][2];
    C[2][2] = A[2][0]*A[0][2] + A[2][1]*A[1][2] + A[2][2]*A[2][2];
}


/* A x ~A = C */
void
Mat3SqrTrans2(double **C, const double **A)
{
    C[0][0] = A[0][0]*A[0][0] + A[0][1]*A[0][1] + A[0][2]*A[0][2];
    C[1][0] = A[1][0]*A[0][0] + A[1][1]*A[0][1] + A[1][2]*A[0][2];
    C[2][0] = A[2][0]*A[0][0] + A[2][1]*A[0][1] + A[2][2]*A[0][2];

    C[0][1] = A[0][0]*A[1][0] + A[0][1]*A[1][1] + A[0][2]*A[1][2];
    C[1][1] = A[1][0]*A[1][0] + A[1][1]*A[1][1] + A[1][2]*A[1][2];
    C[2][1] = A[2][0]*A[1][0] + A[2][1]*A[1][1] + A[2][2]*A[1][2];

    C[0][2] = A[0][0]*A[2][0] + A[0][1]*A[2][1] + A[0][2]*A[2][2];
    C[1][2] = A[1][0]*A[2][0] + A[1][1]*A[2][1] + A[1][2]*A[2][2];
    C[2][2] = A[2][0]*A[2][0] + A[2][1]*A[2][1] + A[2][2]*A[2][2];
}


/* ~A x A = C */
void
Mat3SqrTrans1(double **C, const double **A)
{
    C[0][0] = A[0][0]*A[0][0] + A[1][0]*A[1][0] + A[2][0]*A[2][0];
    C[1][0] = A[0][1]*A[0][0] + A[1][1]*A[1][0] + A[2][1]*A[2][0];
    C[2][0] = A[0][2]*A[0][0] + A[1][2]*A[1][0] + A[2][2]*A[2][0];

    C[0][1] = A[0][0]*A[0][1] + A[1][0]*A[1][1] + A[2][0]*A[2][1];
    C[1][1] = A[0][1]*A[0][1] + A[1][1]*A[1][1] + A[2][1]*A[2][1];
    C[2][1] = A[0][2]*A[0][1] + A[1][2]*A[1][1] + A[2][2]*A[2][1];

    C[0][2] = A[0][0]*A[0][2] + A[1][0]*A[1][2] + A[2][0]*A[2][2];
    C[1][2] = A[0][1]*A[0][2] + A[1][1]*A[1][2] + A[2][1]*A[2][2];
    C[2][2] = A[0][2]*A[0][2] + A[1][2]*A[1][2] + A[2][2]*A[2][2];
}


/* ~A x ~A = C */
void
Mat3TransSqr(double **C, const double **A)
{
    C[0][0] = A[0][0]*A[0][0] + A[0][1]*A[1][0] + A[0][2]*A[2][0];
    C[0][1] = A[1][0]*A[0][0] + A[1][1]*A[1][0] + A[1][2]*A[2][0];
    C[0][2] = A[2][0]*A[0][0] + A[2][1]*A[1][0] + A[2][2]*A[2][0];

    C[1][0] = A[0][0]*A[0][1] + A[0][1]*A[1][1] + A[0][2]*A[2][1];
    C[1][1] = A[1][0]*A[0][1] + A[1][1]*A[1][1] + A[1][2]*A[2][1];
    C[1][2] = A[2][0]*A[0][1] + A[2][1]*A[1][1] + A[2][2]*A[2][1];

    C[2][0] = A[0][0]*A[0][2] + A[0][1]*A[1][2] + A[0][2]*A[2][2];
    C[2][1] = A[1][0]*A[0][2] + A[1][1]*A[1][2] + A[1][2]*A[2][2];
    C[2][2] = A[2][0]*A[0][2] + A[2][1]*A[1][2] + A[2][2]*A[2][2];
}


/* ~A x B = C */
void
Mat3MultTransA(double **C, const double **A, const double **B)
{
    C[0][0] = A[0][0]*B[0][0] + A[1][0]*B[1][0] + A[2][0]*B[2][0];
    C[1][0] = A[0][1]*B[0][0] + A[1][1]*B[1][0] + A[2][1]*B[2][0];
    C[2][0] = A[0][2]*B[0][0] + A[1][2]*B[1][0] + A[2][2]*B[2][0];

    C[0][1] = A[0][0]*B[0][1] + A[1][0]*B[1][1] + A[2][0]*B[2][1];
    C[1][1] = A[0][1]*B[0][1] + A[1][1]*B[1][1] + A[2][1]*B[2][1];
    C[2][1] = A[0][2]*B[0][1] + A[1][2]*B[1][1] + A[2][2]*B[2][1];

    C[0][2] = A[0][0]*B[0][2] + A[1][0]*B[1][2] + A[2][0]*B[2][2];
    C[1][2] = A[0][1]*B[0][2] + A[1][1]*B[1][2] + A[2][1]*B[2][2];
    C[2][2] = A[0][2]*B[0][2] + A[1][2]*B[1][2] + A[2][2]*B[2][2];
}


/* A x ~B = C */
void
Mat3MultTransB(double **C, const double **A, const double **B)
{
    C[0][0] = A[0][0]*B[0][0] + A[0][1]*B[0][1] + A[0][2]*B[0][2];
    C[1][0] = A[1][0]*B[0][0] + A[1][1]*B[0][1] + A[1][2]*B[0][2];
    C[2][0] = A[2][0]*B[0][0] + A[2][1]*B[0][1] + A[2][2]*B[0][2];

    C[0][1] = A[0][0]*B[1][0] + A[0][1]*B[1][1] + A[0][2]*B[1][2];
    C[1][1] = A[1][0]*B[1][0] + A[1][1]*B[1][1] + A[1][2]*B[1][2];
    C[2][1] = A[2][0]*B[1][0] + A[2][1]*B[1][1] + A[2][2]*B[1][2];

    C[0][2] = A[0][0]*B[2][0] + A[0][1]*B[2][1] + A[0][2]*B[2][2];
    C[1][2] = A[1][0]*B[2][0] + A[1][1]*B[2][1] + A[1][2]*B[2][2];
    C[2][2] = A[2][0]*B[2][0] + A[2][1]*B[2][1] + A[2][2]*B[2][2];
}


void
Mat3Add(double **C, const double **A, const double **B)
{
    C[0][0] = A[0][0] + B[0][0];
    C[1][0] = A[1][0] + B[1][0];
    C[2][0] = A[2][0] + B[2][0];

    C[0][1] = A[0][1] + B[0][1];
    C[1][1] = A[1][1] + B[1][1];
    C[2][1] = A[2][1] + B[2][1];

    C[0][2] = A[0][2] + B[0][2];
    C[1][2] = A[1][2] + B[1][2];
    C[2][2] = A[2][2] + B[2][2];
}


void
Mat3Sub(double **A, double **B, double **C)
{
    C[0][0] = A[0][0] - B[0][0];
    C[1][0] = A[1][0] - B[1][0];
    C[2][0] = A[2][0] - B[2][0];

    C[0][1] = A[0][1] - B[0][1];
    C[1][1] = A[1][1] - B[1][1];
    C[2][1] = A[2][1] - B[2][1];

    C[0][2] = A[0][2] - B[0][2];
    C[1][2] = A[1][2] - B[1][2];
    C[2][2] = A[2][2] - B[2][2];
}


void
Mat3TransposeIp(double **matrix)
{
    double tmp;

    tmp          = matrix[0][1];
    matrix[0][1] = matrix[1][0];
    matrix[1][0] = tmp;
    tmp          = matrix[0][2];
    matrix[0][2] = matrix[2][0];
    matrix[2][0] = tmp;
    tmp          = matrix[2][1];
    matrix[2][1] = matrix[1][2];
    matrix[1][2] = tmp;
}


void
Mat3TransposeOp(double **matrix2, const double **matrix1)
{
    matrix2[0][0] = matrix1[0][0];
    matrix2[0][1] = matrix1[1][0];
    matrix2[0][2] = matrix1[2][0];
    matrix2[1][0] = matrix1[0][1];
    matrix2[1][1] = matrix1[1][1];
    matrix2[1][2] = matrix1[2][1];
    matrix2[2][0] = matrix1[0][2];
    matrix2[2][1] = matrix1[1][2];
    matrix2[2][2] = matrix1[2][2];
}


double
Mat2Det(const double **matrix)
{
    return (matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]);
}


double
Mat2DetVals(const double a, const double d, const double b, const double c)
{
    return (a*d - b*c);
}


double
Mat3Det(const double **matrix)
{
    double det;

    det =  matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1])
         - matrix[1][0] * (matrix[0][1] * matrix[2][2] - matrix[0][2] * matrix[2][1])
         + matrix[2][0] * (matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1]);

    return (det);
}


void
Mat3Invert(double **outmat, const double **inmat)
{
    double          invdet = 1.0 / Mat3Det(inmat);
    int             i, j;

    outmat[0][0] = Mat2DetVals(inmat[1][1], inmat[2][2], inmat[1][2], inmat[2][1]);
    outmat[0][1] = Mat2DetVals(inmat[0][2], inmat[2][1], inmat[1][0], inmat[0][1]);
    outmat[0][2] = Mat2DetVals(inmat[0][1], inmat[1][2], inmat[0][2], inmat[2][0]);

    outmat[1][0] = Mat2DetVals(inmat[1][2], inmat[2][0], inmat[1][0], inmat[2][2]);
    outmat[1][1] = Mat2DetVals(inmat[0][0], inmat[2][2], inmat[0][2], inmat[2][0]);
    outmat[1][2] = Mat2DetVals(inmat[0][2], inmat[1][0], inmat[0][0], inmat[1][2]);

    outmat[2][0] = Mat2DetVals(inmat[1][0], inmat[2][1], inmat[1][1], inmat[2][0]);
    outmat[2][1] = Mat2DetVals(inmat[0][1], inmat[1][1], inmat[0][1], inmat[1][0]);
    outmat[2][2] = Mat2DetVals(inmat[0][0], inmat[1][1], inmat[0][1], inmat[1][0]);

    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j)
            outmat[i][j] *= invdet;
}


void
Mat3SymInvert(double **outmat, const double **inmat)
{
    double          invdet = 1.0 / Mat3Det(inmat);

    outmat[0][0] = invdet * Mat2DetVals(inmat[1][1], inmat[2][2], inmat[1][2], inmat[2][1]);
    outmat[1][0] = outmat[0][1] = invdet * Mat2DetVals(inmat[0][2], inmat[2][1], inmat[1][0], inmat[0][1]);
    outmat[2][0] = outmat[0][2] = invdet * Mat2DetVals(inmat[0][1], inmat[1][2], inmat[0][2], inmat[2][0]);

    outmat[1][1] = invdet * Mat2DetVals(inmat[0][0], inmat[2][2], inmat[0][2], inmat[2][0]);
    outmat[2][1] = outmat[1][2] = invdet * Mat2DetVals(inmat[0][2], inmat[1][0], inmat[0][0], inmat[1][2]);

    outmat[2][2] = invdet * Mat2DetVals(inmat[0][0], inmat[1][1], inmat[0][1], inmat[1][0]);
}


void
Mat3MultVec(double *outv, const double **inmat, const double *vec)
{
    int             i, j;

    for (i = 0; i < 3; ++i)
        outv[i] = 0.0;

    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j)
            outv[i] += inmat[i][j] * vec[j];
}


int
VerifyRotMat(double **rotmat, double tol)
{
    int             i, j, k;
    double          sum1, sum2, error;
    double        **testmat = MatAlloc(3, 3);

    for (i = 0; i < 3; ++i)
    {
        sum1 = sum2 = 0.0;
        for (j = 0; j < 3; ++j)
        {
            sum1 += (rotmat[i][j] * rotmat[i][j]);
            sum2 += (rotmat[j][i] * rotmat[j][i]);
        }

        if (fabs(sum1 - 1.0) > tol || fabs(sum2 - 1.0) > tol)
        {
            MatDestroy(&testmat);
            printf(" ERROR: rotation matrix not normalized\n");
            printf("        row %d sum = %f, column %d sum = %f\n", i, sum1, j, sum2);
            fflush(NULL);

            return(0);
        }
    }

    for (i = 0; i < 3; ++i)
    {
        for (j = 0; j < 3; ++j)
        {
            testmat[i][j] = 0.0;
            for (k = 0; k < 3; ++k)
                testmat[i][j] += (rotmat[i][k] * rotmat[j][k]);
        }
    }

    for (i = 0; i < 3; ++i)
    {
        for (j = 0; j < 3; ++j)
        {
            error = fabs(testmat[i][j] - 0.0);
            if (i != j && error > tol)
            {
                printf(" ERROR: rotation matrix not orthogonal\n");
                printf("        off diag error = %e\n", error);
                printf(" Matrix multiplied by self-transpose should be identity:\n");
                Mat3Print(testmat);
                fflush(NULL);
                MatDestroy(&testmat);
                return(0);
            }

            error = fabs(testmat[i][j] - 1.0);
            if (i == j && error > tol)
            {
                printf(" ERROR: rotation matrix not orthogonal\n");
                printf("        on diag error = %e\n", error);
                printf(" Matrix multiplied by self-transpose should be identity:\n");
                Mat3Print(testmat);
                fflush(NULL);
                MatDestroy(&testmat);
                return(0);
            }
        }
    }

    MatDestroy(&testmat);

    return(1);
}


/* returns the closest orthogonal, normalized, rotation matrix to the
   input matrix */
void
ClosestRotMatIp(double **inmat)
{
    double        **u = MatAlloc(3, 3);
    double        **vt = MatAlloc(3, 3);
    double         *s = malloc(3 * sizeof(double));
    Mat3Print(inmat);

    svdGSLDest(inmat, 3, s, vt);
    Mat3TransposeIp(vt);
    Mat3Cpy(u, (const double **) inmat);

    // dgesvd_opt_dest(inmat, 3, 3, u, s, vt);

    /* this guarantees that the determinant of the rot mat is positive,
       as required */
    s[0] = s[1] = 1.0;
    s[2] = Mat3Det((const double **)u) * Mat3Det((const double **) vt);

    Mat3MultUSVOp(inmat, (const double **) u, s, (const double **) vt);

    Mat3Print(inmat);

    MatDestroy(&u);
    MatDestroy(&vt);

    free(s);
}


/* Converts an orthogonal 3x3 rotation matrix to its axis/angle
   representation.  It returns the angle, and the axis unit
   vector is supplied in v[]
   Based on:
   http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToAngle/index.htm
*/
double
RotMat2AxisAngle(double **rot, double *v)
{
    double          epsilon = FLT_EPSILON;
    double          angle, x, y, z, s, tx, ty, tz;
    int             xZero, yZero, zZero, xyPos, xzPos, yzPos;

//MatPrint(rot, 3);

    if (fabs(rot[0][1] - rot[1][0]) < epsilon &&
        fabs(rot[0][2] - rot[2][0]) < epsilon &&
        fabs(rot[1][2] - rot[2][1]) < epsilon)
    {// singularity found
        if (rot[0][0] > 0.0 && rot[1][1] > 0.0 && rot[2][2] > 0.0)
        {
            // this singularity is identity matrix so angle = 0
            // note epsilon is greater in this case since we only have to distinguish between 0 and 180 degrees
            //angle = 0.0;
            v[0] = 1.0;  // axis is arbitrary
            v[1] = 0.0;
            v[2] = 0.0;

            return(0.0);
        }
        else
        {
            // otherwise this singularity is angle = 180
            angle = MY_PI;

            x = 0.5 * (rot[0][0] + 1.0);

            if (x > 0.0) // can only take square root of positive number, always true for orthogonal matrix
                x = sqrt(x);
            else
                x = 0.0; // in case matrix has become de-orthogonalised

            y = 0.5 * (rot[1][1] + 1.0);

            if (y > 0.0) // can only take square root of positive number, always true for orthogonal matrix
                y = sqrt(y);
            else
                y = 0.0; // in case matrix has become de-orthogonalised

            z = 0.5 * (rot[2][2] + 1.0);

            if (z > 0.0) // can only take square root of positive number, always true for orthogonal matrix
                z = sqrt(z);
            else
                z = 0.0; // in case matrix has become de-orthogonalised

            xZero = (fabs(x) < epsilon);
            yZero = (fabs(y) < epsilon);
            zZero = (fabs(z) < epsilon);
            xyPos = (rot[0][1] > 0.0);
            xzPos = (rot[0][2] > 0.0);
            yzPos = (rot[1][2] > 0.0);

            if (xZero && !yZero && !zZero) // implements  last 6 rows of above table
            {
                if (!yzPos)
                    y = -y;
            }
            else if (yZero && !zZero)
            {
                if (!xzPos)
                    z = -z;
            }
            else if (zZero)
            {
                if (!xyPos)
                    x = -x;
            }

            v[0] = x;
            v[1] = y;
            v[2] = z;

            return(angle);
        }
    }
    else
    {
        tx = rot[2][1] - rot[1][2];
        ty = rot[0][2] - rot[2][0];
        tz = rot[1][0] - rot[0][1];
        s = sqrt(tx*tx + ty*ty + tz*tz); // used to normalise

        if (fabs(s) < epsilon)
            s = 1.0; // prevent divide by zero, should not happen if matrix is orthogonal

        angle = acos(0.5 * (rot[0][0] + rot[1][1] + rot[2][2] - 1.0));
        x = (rot[2][1] - rot[1][2]) / s;
        y = (rot[0][2] - rot[2][0]) / s;
        z = (rot[1][0] - rot[0][1]) / s;

        v[0] = x;
        v[1] = y;
        v[2] = z;

        return(angle);
    }
}


/* Converts an orthogonal 3x3 rotation matrix to its axis/angle
   representation.  It returns the angle, and the axis unit
   vector is supplied in v[]
   Based on:
   http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToAngle/index.htm
   First converts a rot matrix to a quaternion, then to the angle/axis representation.
   NB: Still has some singularity problems at 180 degrees.
*/
double
RotMat2AxisAngleQuat(double **rot, double *v)
{
    double              trace, angle, s, t, invt, w, x, y, z;

    /* convert to quaternion */
    trace = rot[0][0] + rot[1][1] + rot[2][2] + 1.0;

    if( trace > FLT_EPSILON )
    {
        s = 0.5 / sqrt(trace);
        w = 0.25 / s;
        x = ( rot[2][1] - rot[1][2] ) * s;
        y = ( rot[0][2] - rot[2][0] ) * s;
        z = ( rot[1][0] - rot[0][1] ) * s;
    }
    else
    {
        if (rot[0][0] > rot[1][1] && rot[0][0] > rot[2][2])
        {
            s = 2.0 * sqrt( 1.0 + rot[0][0] - rot[1][1] - rot[2][2]);
            x = 0.25 * s;
            y = (rot[0][1] + rot[1][0] ) / s;
            z = (rot[0][2] + rot[2][0] ) / s;
            w = (rot[1][2] - rot[2][1] ) / s;
        }
        else if (rot[1][1] > rot[2][2])
        {
            s = 2.0 * sqrt(1.0 + rot[1][1] - rot[0][0] - rot[2][2]);
            x = (rot[0][1] + rot[1][0] ) / s;
            y = 0.25 * s;
            z = (rot[1][2] + rot[2][1] ) / s;
            w = (rot[0][2] - rot[2][0] ) / s;
        }
        else
        {
            s = 2.0 * sqrt(1.0 + rot[2][2] - rot[0][0] - rot[1][1]);
            x = (rot[0][2] + rot[2][0] ) / s;
            y = (rot[1][2] + rot[2][1] ) / s;
            z = 0.25 * s;
            w = (rot[0][1] - rot[1][0] ) / s;
        }
    }

    /* Now convert that quaternion to angle/axis */
    angle = 2.0 * acos(w);
    t = sqrt(1.0 - w * w); // assuming quaternion normalised then w is less than 1, so term always positive.

    if (t < FLT_EPSILON)
    {// test to avoid divide by zero, s is always positive due to sqrt
        v[0] = 1.0;  // if s close to zero then direction of axis not important
        v[1] = 0.0;
        v[2] = 0.0;
    }
    else
    {
        invt = 1.0 / t;
        v[0] = invt * x; // normalise axis
        v[1] = invt * y;
        v[2] = invt * z;
    }

    return(angle);
}
