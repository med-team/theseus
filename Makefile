# Do not modify this file
# modify make.inc instead for platform specific compilation

include make.inc

OBJECTS = CovMat.o DistMat.o Embed.o  Error.o FragCds.o FragDist.o HierarchVars.o \
lodmats.o MultiPose.o MultiPose2MSA.o MultiPosePth.o \
PCAstats.o pdbIO.o pdbMalloc.o pdbStats.o pdbUtils.o QuarticHornFrag.o \
RandCds.o pdbSSM.o \
ProcGSLSVD.o ProcGSLSVDNu.o ProcJacobiSVD.o \
GibbsMet.o \
qcprot.o \
StructAlign.o NWfill.o \
termcol.o theseuslib.o



all:				libs ltheseus progs

libs:				ldistfit lDLTutils lmsa ldltmath ldssplite

progs:				theseus libs

crushexe:			libs ltheseus crush

ldistfit:
					( cd libdistfit; $(MAKE) && cp libdistfit.a ../lib  )

lDLTutils:
					( cd libDLTutils; $(MAKE) && cp libDLTutils.a ../lib  )

lmsa:
					( cd libmsa; $(MAKE) && cp libmsa.a ../lib  )

ldltmath:
					( cd libdltmath; $(MAKE) && cp libdltmath.a ../lib  )

ldssplite:
					( cd libdssplite; $(MAKE) && cp libdssplite.a ../lib  )

distfitexe:
					$(CC) $(OPT) $(CFLAGS) -c distfit.c
					$(CC) $(OPT) $(CFLAGS) $(LDFLAGS) $(LIBDIR) distfit.o $(LIBS) $(SYSLIBS) -o distfit


# CRUSH-specific files

crush:	    		crush.o $(OBJECTS)
					$(CC) $(OPT) $(CFLAGS) $(LDFLAGS) $(LIBDIR) crush.o $(OBJECTS) $(LIBS) $(SYSLIBS) -o crush

crush.o:			crush.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c crush.c

NWfill.o:    		NWfill.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c NWfill.c

StructAlign.o:    	StructAlign.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c StructAlign.c


# THESEUS-specific files
theseus:			theseus.o $(OBJECTS)
					$(CC) $(OPT) $(CFLAGS) $(LDFLAGS) $(LIBDIR) theseus.o $(OBJECTS) $(LIBS) $(SYSLIBS) -o theseus

CovMat.o:			CovMat.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c CovMat.c

DistMat.o:			DistMat.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c DistMat.c

Error.o:			Error.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c Error.c

FragCds.o:			FragCds.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c FragCds.c

FragDist.o:			FragDist.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c FragDist.c

HierarchVars.o:		HierarchVars.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c HierarchVars.c

lodmats.o:			lodmats.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c lodmats.c

Embed.o:			Embed.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c Embed.c

MultiPose.o:		MultiPose.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c MultiPose.c

MultiPose2MSA.o:	MultiPose2MSA.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c MultiPose2MSA.c

MultiPosePth.o:		MultiPosePth.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c MultiPosePth.c

PCAstats.o:			PCAstats.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c PCAstats.c

pdbIO.o:			pdbIO.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c pdbIO.c

pdbMalloc.o:		pdbMalloc.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c pdbMalloc.c

pdbStats.o:			pdbStats.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c pdbStats.c

pdbSSM.o:			pdbSSM.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c pdbSSM.c

pdbUtils.o:			pdbUtils.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c pdbUtils.c

QuarticHornFrag.o:	QuarticHornFrag.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c QuarticHornFrag.c

RandCds.o:			RandCds.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c RandCds.c

GibbsMet.o: 		GibbsMet.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c GibbsMet.c


# Superposition algorithms
Kabsch.o:			Kabsch.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c Kabsch.c

# Kearsley.o:			Kearsley.c
# 					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c Kearsley.c
#
# Horn.o:				Horn.c
# 					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c Horn.c

ProcJacobiSVD.o:	ProcJacobiSVD.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c ProcJacobiSVD.c
#
# ProcJacobiSVDOcc.o:	ProcJacobiSVDOcc.c
# 					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c ProcJacobiSVDOcc.c

ProcGSLSVD.o:		ProcGSLSVD.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c ProcGSLSVD.c

ProcGSLSVDNu.o:		ProcGSLSVDNu.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c ProcGSLSVDNu.c

termcol.o:  		termcol.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c termcol.c

qcprot.o:			qcprot.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c qcprot.c

theseus.o:			theseus.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c theseus.c

theseuslib.o:		theseuslib.c
					$(CC) $(OPT) $(CFLAGS) $(INCDIR) -c theseuslib.c

ltheseus:			$(OBJECTS)
					$(ARCH) $(ARCHFLAGS) libtheseus.a $(OBJECTS)
					mv libtheseus.a lib/libtheseus.a
					$(RANLIB) lib/libtheseus.a

install:
					chmod +x theseus theseus_align
					cp theseus theseus_align $(INSTALLDIR)

installlibs:
					cp lib/*.a ${LOCALLIBDIR}

clean:
					find . -name '*.[oa]' -exec rm -f {} \;
					rm -f theseus distfit

