/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef MYASSERT_SEEN
    #define MYASSERT_SEEN

    #ifdef NDEBUG
        #define myassert(e)   ((void)0)
    #else

        #include <stdlib.h>

        #define myassert(e)  \
            ((void) ((e) ? 0 : __myassert (#e, __FILE__, __LINE__, __func__)))
        #define __myassert(e, file, line, function) \
            ((void)printf ("\n%s:%u: failed assertion `%s'\nin function `%s'\n\n", file, line, e, function), abort(), 0)

    #endif
#endif
