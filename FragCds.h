/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef FRAGCOORDS_SEEN
#define FRAGCOORDS_SEEN

typedef struct FragCds_
{
    int             fraglen;   /* number of coordinates */
    double         *x, *y, *z; /* x,y,z atomic coordinates */
    double         *w;         /* weight = o/b */
    double         *var;
    double          center[3]; /* centroid of coordinates */
} FragCds;

FragCds
*FragCdsAlloc(int fraglen);

void
FragCdsFree(FragCds **frag_ptr);

void
CenterFrag(FragCds *frag);

void
CenterFragCA(FragCds *frag);

double
DiffDist(FragCds *frag1, FragCds *frag2, double **distmat1, double **distmat2);

double
SqrFragCdsDist(FragCds *cds1, int atom1, FragCds *cds2, int atom2);

double
RadGyrSqrFrag(const FragCds *frag);

void
PrintFragCds(FragCds *cds);

#endif
