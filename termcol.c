/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#include <stdlib.h>

#ifdef _WIN32
    /* regular */
    const char tc_black[]  = "";
    const char tc_red[]    = "";
    const char tc_green[]  = "";
    const char tc_yellow[] = "";
    const char tc_blue[]   = "";
    const char tc_purple[] = "";
    const char tc_cyan[]   = "";
    const char tc_white[]  = "";

    /* bold (xterm) or light (console) */
    const char tc_BLACK[]  = "";
    const char tc_RED[]    = "";
    const char tc_GREEN[]  = "";
    const char tc_YELLOW[] = "";
    const char tc_BLUE[]   = "";
    const char tc_PURPLE[] = "";
    const char tc_CYAN[]   = "";
    const char tc_WHITE[]  = "";

    /* underline */
    const char tc_ublack[]  = "";
    const char tc_ured[]    = "";
    const char tc_ugreen[]  = "";
    const char tc_uyellow[] = "";
    const char tc_ublue[]   = "";
    const char tc_upurple[] = "";
    const char tc_ucyan[]   = "";
    const char tc_uwhite[]  = "";

    /* blink */
    const char tc_bblack[]  = "";
    const char tc_bred[]    = "";
    const char tc_bgreen[]  = "";
    const char tc_byellow[] = "";
    const char tc_bblue[]   = "";
    const char tc_bpurple[] = "";
    const char tc_bcyan[]   = "";
    const char tc_bwhite[]  = "";

    /* inverse */
    const char tc_iblack[]  = "";
    const char tc_ired[]    = "";
    const char tc_igreen[]  = "";
    const char tc_iyellow[] = "";
    const char tc_iblue[]   = "";
    const char tc_ipurple[] = "";
    const char tc_icyan[]   = "";
    const char tc_iwhite[]  = "";

    /* concealed */
    const char tc_cblack[]  = "";
    const char tc_cred[]    = "";
    const char tc_cgreen[]  = "";
    const char tc_cyellow[] = "";
    const char tc_cblue[]   = "";
    const char tc_cpurple[] = "";
    const char tc_ccyan[]   = "";
    const char tc_cwhite[]  = "";

    const char tc_NC[]     = "";
#else
    /* regular */
    const char tc_black[]  = "\033[0;30m";
    const char tc_red[]    = "\033[0;31m";
    const char tc_green[]  = "\033[0;32m";
    const char tc_yellow[] = "\033[0;33m";
    const char tc_blue[]   = "\033[0;34m";
    const char tc_purple[] = "\033[0;35m";
    const char tc_cyan[]   = "\033[0;36m";
    const char tc_white[]  = "\033[0;37m";

    /* bold (xterm) or light (console) */
    const char tc_BLACK[]  = "\033[1;30m";
    const char tc_RED[]    = "\033[1;31m";
    const char tc_GREEN[]  = "\033[1;32m";
    const char tc_YELLOW[] = "\033[1;33m";
    const char tc_BLUE[]   = "\033[1;34m";
    const char tc_PURPLE[] = "\033[1;35m";
    const char tc_CYAN[]   = "\033[1;36m";
    const char tc_WHITE[]  = "\033[1;37m";

    /* underline */
    const char tc_ublack[]  = "\033[4;30m";
    const char tc_ured[]    = "\033[4;31m";
    const char tc_ugreen[]  = "\033[4;32m";
    const char tc_uyellow[] = "\033[4;33m";
    const char tc_ublue[]   = "\033[4;34m";
    const char tc_upurple[] = "\033[4;35m";
    const char tc_ucyan[]   = "\033[4;36m";
    const char tc_uwhite[]  = "\033[4;37m";

    /* blink */
    const char tc_bblack[]  = "\033[5;30m";
    const char tc_bred[]    = "\033[5;31m";
    const char tc_bgreen[]  = "\033[5;32m";
    const char tc_byellow[] = "\033[5;33m";
    const char tc_bblue[]   = "\033[5;34m";
    const char tc_bpurple[] = "\033[5;35m";
    const char tc_bcyan[]   = "\033[5;36m";
    const char tc_bwhite[]  = "\033[5;37m";

    /* inverse */
    const char tc_iblack[]  = "\033[7;30m";
    const char tc_ired[]    = "\033[7;31m";
    const char tc_igreen[]  = "\033[7;32m";
    const char tc_iyellow[] = "\033[7;33m";
    const char tc_iblue[]   = "\033[7;34m";
    const char tc_ipurple[] = "\033[7;35m";
    const char tc_icyan[]   = "\033[7;36m";
    const char tc_iwhite[]  = "\033[7;37m";

    /* concealed */
    const char tc_cblack[]  = "\033[8;30m";
    const char tc_cred[]    = "\033[8;31m";
    const char tc_cgreen[]  = "\033[8;32m";
    const char tc_cyellow[] = "\033[8;33m";
    const char tc_cblue[]   = "\033[8;34m";
    const char tc_cpurple[] = "\033[8;35m";
    const char tc_ccyan[]   = "\033[8;36m";
    const char tc_cwhite[]  = "\033[8;37m";

    const char tc_NC[]     = "\033[0m";
#endif
