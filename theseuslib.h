/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef THESEUSLIB_SEEN
#define THESEUSLIB_SEEN

#include "Cds.h"

int
test_charmm(CdsArray *cdsA);

void
CalcS2(CdsArray *cdsA, const int nsell, double *bxij, double *byij, double *bzij,
       double *rij, double *s2, const int whoiam);

int
theseuss2_(const double *xbuf, const double *ybuf, const double *zbuf,
           const int *natomx, const int *nensem, const int *nsell,
           double *bxij, double *byij, double *bzij,
           const int *il, const int *jl, double *rij, double *s2,
           const int *ls, const int *whoiam);

int
theseus_(double *xbuf, double *ybuf, double *zbuf, int *natomx, int *nensem, int *ls);

int
MultiPoseLib(CdsArray *cdsA);

#endif
