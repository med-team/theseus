/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef PDBIO_SEEN
#define PDBIO_SEEN

#include "pdbMalloc.h"

int
ReadPDBCds(char *pdbfile_name, PDBCdsArray *pdbA, int cds_i, int modelnum, int amber, int fix_atom_names);

int
ReadTPSCds(char *pdbfile_name, PDBCdsArray *pdbA, int cds_i, int modelnum);

PDBCdsArray
*GetTPSCds(char **argv_array, int narguments);

void
GetCdsSelection(CdsArray *baseA, PDBCdsArray *pdbA);

PDBCdsArray
*GetPDBCds(char **argv_array, int narguments, int fmodel, int amber, int fix_atom_names);

void
PrintCds(Cds *cds);

void
PrintPDBCds(FILE *pdbfile, PDBCds *pdbcds);

char
*WriteFileMap(PDBCdsArray *myPDBs, const char *filemap_name);

void
WriteModelFile(PDBCdsArray *pdbA, char *outfile_name);

void
WriteTheseusTPSModelFile(PDBCdsArray *pdbA, char *outfile_name);

void
WriteAveTPSCdsFile(PDBCdsArray *pdbA, char *outfile_name);

void
WriteTheseusModelFile(PDBCdsArray *pdbA, Algorithm *algo, Statistics *stats, char *outfile_name);

void
WriteTheseusModelFileNoStats(PDBCdsArray *pdbA, Algorithm *algo, char *outfile_name);

void
WriteTheseusCdsModelFile(CdsArray *cdsA, char *outfile_name);

void
OverWriteTheseusCdsModelFile(CdsArray *cdsA, char *outfile_name);

void
WriteTheseusPDBFiles(PDBCdsArray *pdbA, Algorithm *algo, Statistics *stats, char *fname);

void
WriteOlveModelFile(PDBCdsArray *pdbA, Algorithm *algo, Statistics *stats, char *outfile_name);

void
WriteCdsFile(Cds *cds, char *outfile_name);

void
WriteAveCdsFile(CdsArray *cdsA, char *outfile_name);

void
WriteAveCds(CdsArray *cdsA, char *outfile_name);

void
WriteAvePDBCdsFile(PDBCdsArray *pdbA, char *outfile_name);

void
WriteBinPDBCdsArray(PDBCdsArray *pdbA);

PDBCdsArray
*ReadBinPDBCdsArray(char *filename);

void
WriteBinPDBCds(PDBCds *pdbcds, FILE *fp);

void
ReadBinPDBCds(PDBCds *pdbcds, FILE *fp);

void
WriteBinMatrix(double **mat, int rows, int cols, FILE *fp);

void
ReadBinMatrix(double **mat, FILE *fp);

int
ConvertLele_freeform(char *fp_name, const int dim, const int forms, const int lmarks);

int
ConvertDryden(char *fp_name, const int dim, const int forms, const int lmarks);

int
ConvertLele(char *fp_name, const int dim, const int forms, const int lmarks);

void
WriteLeleModelFile(PDBCdsArray *pdbAr);

void
WriteInstModelFile(char *fext, CdsArray *cdsA);

void
WriteEdgarSSM(CdsArray *cdsA);

void
WriteOlveFiles(CdsArray *cdsA);

void
WriteDistMatTree(CdsArray *cdsA);

#endif
