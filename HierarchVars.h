/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2015 Douglas L. Theobald

    This file is part of THESEUS.

    THESEUS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    THESEUS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with THESEUS in the file 'COPYING'. If not, see
    <http://www.gnu.org/licenses/>.

    -/_|:|_|_\-
*/

#ifndef HIERARCHVARS_SEEN
#define HIERARCHVARS_SEEN

void
InvGammaFitEvals(CdsArray *cdsA, int iterate);

void
InvGammaAdjustVarPhiNu(Cds **cds, double *newvar, const int vlen, const int cnum,
                       double *var, const double phi, const double n);

void
InvGammaFitMarginalGSLBrent(CdsArray *cdsA);

void
InvGammaFitMarginalMLPhi(CdsArray *cdsA);

void
InvGammaFitMarginalGSLRoot(CdsArray *cdsA);

void
InvGammaFitEvalsEMFixedC(CdsArray *cdsA, const double c, int iterate);

void
InvGammaFitEvalsML(CdsArray *cdsA, int iterate);

#endif
